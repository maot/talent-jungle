package services

import (
	"gopkg.in/mgo.v2/bson"
	"sort"
)

type SortPool []bson.M

func (sp SortPool) Len() int           { return len(sp) }
func (sp SortPool) Less(i, j int) bool { return sp[i]["value"].(int) > sp[j]["value"].(int) }
func (sp SortPool) Swap(i, j int)      { sp[i], sp[j] = sp[j], sp[i] }

func Group(items []bson.M) (ret []bson.ObjectId) {
	sp := SortPool{}
	word2index := map[bson.ObjectId]int{}
	for _, item := range items {
		if index := word2index[item["_id"].(bson.ObjectId)]; index == 0 {
			word2index[item["_id"].(bson.ObjectId)] = len(word2index) + 1
			sp = append(sp, bson.M{"key": item["_id"], "value": 1})
		} else {
			sp[index]["value"] = sp[index]["value"].(int) + 1
		}
	}
	sort.Sort(sp)
	ret = []bson.ObjectId{}
	for _, sp_item := range sp {
		ret = append(ret, sp_item["key"].(bson.ObjectId))
	}
	return
}

func Sort(sp SortPool) SortPool {
	sort.Sort(sp)
	return sp
}
