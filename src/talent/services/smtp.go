package services

import (
	"net/smtp"
	"strings"
)

func auth(username string, password string, smtp_server string) (auth smtp.Auth) {
	auth = smtp.PlainAuth(
		"",
		username,
		password,
		smtp_server,
	)
	return
}

//Send html to single user
func send(auth smtp.Auth, smtp_server string, from string, from_who string, to string, to_who string, subject string, body string) (err error) {

	content_type := "Content-Type: text/html; charset=UTF-8"

	msg := []byte("To: " + to_who + " <" + to + ">\r\nFrom: " + from_who + " <" + from + ">\r\nSubject: " + subject + "\r\n" + content_type + "\r\n\r\n" + body)

	err = smtp.SendMail(
		smtp_server,
		auth,
		from,
		[]string{to},
		msg,
	)

	return
}

func Verify(token string, to string, to_who string) (err error) {
	from := "qicongchen92@gmail.com"
	password := "308Thukeg"
	from_who := "Edward from Talent Jungle"
	smtp_server := "smtp.gmail.com:587"
	subject := "Welcome to Talent Jungle!"
	body := `
	<html>
	<body>
	<p>Hi ` + to_who + `,</p>
	<p>Welcome to Talent Jungle!</p>
	<p>Click <a href="http://localhost/verify?token=` + token + `">here</a> to finish your signup!</p>
	</body>
	</html>
	`
	auth := auth(from, password, strings.Split(smtp_server, ":")[0])
	err = send(auth, smtp_server, from, from_who, to, to_who, subject, body)
	return
}

// func main() {
// 	println(Signup("haha", "qicongc@cs.cmu.edu", "Qicong Chen"))
// }
