package services

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func Connect() *mgo.Session {
	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	return session
}

func Insert(session *mgo.Session, db string, collection string, data interface{}) error {
	//data must be an address
	return session.DB(db).C(collection).Insert(data)
}

func Remove(session *mgo.Session, db string, collection string, query bson.M) error {
	_, err := session.DB(db).C(collection).RemoveAll(query)
	return err
}

func Count(session *mgo.Session, db string, collection string, query bson.M) (int, error) {
	return session.DB(db).C(collection).Find(query).Count()
}

func All(session *mgo.Session, db string, collection string, query bson.M, results interface{}) error {
	//results must me an address
	return session.DB(db).C(collection).Find(query).Select(bson.M{"_id": 1}).All(results)
}

func Search(session *mgo.Session, db string, collection string, query bson.M, sort []string, offset int, size int, results interface{}) error {
	//results must me an address
	return session.DB(db).C(collection).Find(query).Sort(sort...).Skip(offset).Limit(size).All(results)
}

func One(session *mgo.Session, db string, collection string, query bson.M, result interface{}) error {
	//result must be an address
	return session.DB(db).C(collection).Find(query).One(result)
}

func Update(session *mgo.Session, db string, collection string, query bson.M, update interface{}) error {
	//update must be an address
	return session.DB(db).C(collection).Update(query, update)
}

// func main() {
// 	results := []bson.M{}
// 	session := Connect()
// 	defer session.Close()
// 	query := bson.M{"name": bson.M{"$regex": bson.RegEx{"make", "i"}}}
// 	//query := bson.M{"$or": []bson.M{bson.M{"name": bson.M{"$regex": bson.RegEx{"make", "i"}}}, bson.M{"outline": {"$regex": bson.RegEx{"make", "i"}}}}}
// 	All(session, "talent", "works", query, &results)
// 	log.Println(results)
// }
