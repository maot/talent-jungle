package services

import (
	"errors"
	"gopkg.in/xmlpath.v2"
	"net/http"
)

func ScrapeOne(url string, xpath string) (string, error) {
	response, url_err := http.Get(url)
	if url_err != nil {
		return "", url_err
	}
	defer response.Body.Close()
	path := xmlpath.MustCompile(xpath)
	root, html_err := xmlpath.ParseHTML(response.Body)
	if html_err != nil {
		return "", html_err
	}
	if value, ok := path.String(root); ok {
		return value, nil
	} else {
		return "", errors.New("xpath not found")
	}
}
