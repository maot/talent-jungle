package proto

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Order struct {
	Time Time          `json:"time" bson:"time"`
	ID   bson.ObjectId `json:"id" bson:"id"`
}

type User struct {
	//basic type
	ID        bson.ObjectId `bson:"_id,omitempty"`
	Email     string        `json:"email" bson:"email"`
	Firstname string        `json:"firstname" bson:"firstname"`
	Lastname  string        `json:"lastname" bson:"lastname"`
	Password  string        `json:"pass" bson:"password"`
	Gender    bool          `json:"gender" bson:"gender"`
	Image     string        `json:"image" bson:"image"`

	//other
	Begin time.Time

	//as instructor
	Taught   []bson.ObjectId `json:"taught" bson:"taught"`
	Teaching []bson.ObjectId `json:"teaching" bson:"teaching"`
	Opening  []bson.ObjectId `json:"opening" bson:"opening"`

	//as student
	History []Order `json:"history" bson:"history"`
	Succeed []Order `json:"succeed" bson:"succeed"`
}
