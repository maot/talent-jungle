package proto

import (
	"gopkg.in/mgo.v2/bson"
)

type Popularity struct {
	Rate float32 `bson:"rate" json:"rate"`
	Flow int     `bson:"flow" json:"flow"`
}

type Work struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	Name           string        `json:"name" bson:"name"`
	Outline        string        `json:"outline" bson:"outline"`
	Preparations   []string      `json:"preparations" bson:"preparations"`
	Certifications []string      `json:"certifications" bson:"certifications"`
	Objectives     []string      `json:"objectives" bson:"objectives"`
	Email          string        `json:"email" bson:"email"`
	Examples       int           `json:"examples" bson:"examples"`
	Popularity     Popularity    `json:"popularity" bson:"popularity"`
}
