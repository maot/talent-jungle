package proto

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Syllabus struct {
	Time int    `bson:"time" json:"time"`
	Do   string `bson:"do" json:"do"`
}
type Time struct {
	Time   time.Time `bson:"time" json:"time"`
	String string    `bson:"string" json:"string"`
}
type Capacity struct {
	Min     int `bson:"min" json:"min"`
	Max     int `bson:"max" json:"max"`
	Current int `bson:"current" json:"current"`
}
type Review struct {
	Content string `bson:"content" json:"content"`
	Rate    int    `bson:"rate" json:"rate"`
	Email   string `json:"email" bson:"email"`
	Time    Time   `bson:"time" json:"time"`
}

type Workshop struct {
	ID       bson.ObjectId `bson:"_id,omitempty"`
	Email    string        `bson:"email" json:"email"`
	Workid   bson.ObjectId `bson:"workid" json:"workid"`
	Syllabus []Syllabus    `bson:"syllabus" json:"syllabus"`
	Length   int           `bson:"length" json:"length"`
	Time     Time          `bson:"time" json:"time"`
	Location string        `bson:"location" json:"location"`
	Price    float32       `bson:"price" json:"price"`
	Capacity Capacity      `bson:"capacity" json:"capacity"`
	Reviews  []Review      `bson:"reviews" json:"reviews"`
}
