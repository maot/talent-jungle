package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gopkg.in/mgo.v2/bson"
	"html/template"
	"log"
	"net/http"
	"strings"
	"talent/controller"
)

var view_dir = "web/view/"
var templates = template.Must(template.New("").Delims("<<", ">>").ParseFiles(view_dir+"signup.html",
	view_dir+"home.html", view_dir+"detail.html", view_dir+"order.html",view_dir+"search.html"))
var store = sessions.NewCookieStore([]byte("talentjungle"))

func SignupGet(w http.ResponseWriter, r *http.Request) {
	// if has_login := CheckLogin(r); has_login == true {
	// 	session, _ := store.Get(r, "talent")
	// 	session.AddFlash("You have already logged in.")
	// 	session.Save(r, w)
	// 	http.Redirect(w, r, "/landing", 301)
	// }
	session, _ := store.Get(r, "talent")
	// drain flashes
	flashes := session.Flashes()
	session.Save(r, w)
	// gen page
	data := bson.M{}
	if len(flashes) == 0 {
		data["message"] = ""
	} else {
		data["message"] = flashes[0]
	}
	data["success"] = session.Values["success"]
	err := templates.ExecuteTemplate(w, "signup.html", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func CheckLogin(r *http.Request) bool {
	// TODO: check error
	session, _ := store.Get(r, "talent")
	// Set some session values.
	if session.Values["login"] == nil || session.Values["login"] == false {
		return false
	} else {
		return true
	}
}

func CheckParam(r *http.Request, param string) bool {
	if r.FormValue(param) != "" {
		return true
	}
	return false
}

func LoginPost(w http.ResponseWriter, r *http.Request) {
	form := bson.M{
		"email":    r.FormValue("email"),
		"password": r.FormValue("password"),
	}
	if err := controller.Signin(form); err != nil {
		session, _ := store.Get(r, "talent")
		session.Values["success"] = false
		session.AddFlash(err.Error())
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
	} else {
		session, _ := store.Get(r, "talent")
		session.Values["login"] = true
		session.Values["email"] = form["email"]
		//session.AddFlash("Welcome to Talent Jungle!")
		session.Save(r, w)
		http.Redirect(w, r, "/landing", 301)
	}
}

func Logout(w http.ResponseWriter, r *http.Request) {
	if has_login := CheckLogin(r); has_login == false {
		session, _ := store.Get(r, "talent")
		session.Values["success"] = false
		session.AddFlash("You haven't logged in.")
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
		return
	}
	session, _ := store.Get(r, "talent")
	session.Values["login"] = false
	session.Values["success"] = true
	session.AddFlash("Successfully logged out.")
	session.Save(r, w)
	http.Redirect(w, r, "/signup", 301)
}

func SignupPost(w http.ResponseWriter, r *http.Request) {
	form := bson.M{
		"email":    r.FormValue("email"),
		"password": r.FormValue("password"),
	}
	err := controller.Signup(form)
	if err != nil {
		session, _ := store.Get(r, "talent")
		session.Values["success"] = false
		session.AddFlash(err.Error())
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
	} else {
		session, _ := store.Get(r, "talent")
		session.Values["success"] = true
		session.AddFlash("Email has been sent to " + form["email"].(string))
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
	}
}

func Verify(w http.ResponseWriter, r *http.Request) {
	form := bson.M{
		"token": r.FormValue("token"),
	}
	err := controller.Verify(form)
	if err != nil {
		session, _ := store.Get(r, "talent")
		session.Values["success"] = false
		session.AddFlash(err.Error())
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
	} else {
		session, _ := store.Get(r, "talent")
		session.Values["success"] = true
		session.AddFlash("Successfully verified. Please login.")
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
	}
}

func LandingGet(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "talent")
	// check login
	if has_login := CheckLogin(r); has_login == false {
		session.Values["success"] = false
		session.AddFlash("You haven't logged in.")
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
		return
	}
	// drain flashes
	session.Flashes()
	session.Save(r, w)
	// generate page
	user, _ := controller.GetUser(bson.M{"email": session.Values["email"]})
	data := bson.M{
		"user": user,
	}
	err := templates.ExecuteTemplate(w, "home.html", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func LandingData(w http.ResponseWriter, r *http.Request) {
	form := bson.M{
		"sort":   strings.Split(r.FormValue("sort"), ","),
		"offset": r.FormValue("offset"),
		"size":   r.FormValue("size"),
	}
	log.Println(form)
	ret, err := controller.NewWorkshops(form)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var js []byte
	js, err = json.Marshal(ret)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func SignupData(w http.ResponseWriter, r *http.Request) {
	form := bson.M{
		"email": r.FormValue("email"),
	}
	message, code := controller.CheckEmail(form)
	js, err := json.Marshal(bson.M{"code": code, "message": message})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func SearchGet(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "talent")
	// check login
	if has_login := CheckLogin(r); has_login == false {
		session.Values["success"] = false
		session.AddFlash("You haven't logged in.")
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
		return
	}
	// drain flashes
	session.Flashes()
	session.Save(r, w)
	// generate page
	user, _ := controller.GetUser(bson.M{"email": session.Values["email"]})
	data := bson.M{
		"user": user,
		"query":r.FormValue("query"),
	}
	err := templates.ExecuteTemplate(w, "search.html", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func SearchData(w http.ResponseWriter, r *http.Request) {
	form := bson.M{
		"query":  r.FormValue("query"),
		"sort":   strings.Split(r.FormValue("sort"), ","),
		"offset": r.FormValue("offset"),
		"size":   r.FormValue("size"),
	}
	log.Println(form)
	ret, err := controller.SelectWorkshops(form)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var js []byte
	js, err = json.Marshal(ret)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func MyOrderSucceedData(w http.ResponseWriter, r *http.Request) {
	form := bson.M{
		"email":  r.FormValue("email"),
		"offset": r.FormValue("offset"),
		"size":   r.FormValue("size"),
	}
	ret, err := controller.GetSucceed(form)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var js []byte
	js, err = json.Marshal(bson.M{"total": len(ret), "result": ret})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
func MyOrderHistoryData(w http.ResponseWriter, r *http.Request) {
	form := bson.M{
		"email":  r.FormValue("email"),
		"offset": r.FormValue("offset"),
		"size":   r.FormValue("size"),
	}
	ret, err := controller.GetHistory(form)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var js []byte
	js, err = json.Marshal(bson.M{"total": len(ret), "result": ret})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func WorkShopGet(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "talent")
	// check login
	if has_login := CheckLogin(r); has_login == false {
		session.Values["success"] = false
		session.AddFlash("You haven't logged in.")
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
		return
	}
	// check param
	if CheckParam(r, "id") == false {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	// fetch data
	form := bson.M{
		"id": r.FormValue("id"),
	}
	ret, err := controller.GetWorkshop(form)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// drain flashes
	session.Flashes()
	session.Save(r, w)
	// generate page
	user, _ := controller.GetUser(bson.M{"email": session.Values["email"]})
	data := bson.M{
		"workshop": ret,
		"user":     user,
	}
	err = templates.ExecuteTemplate(w, "detail.html", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func MyOrderGet(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "talent")
	// check login
	if has_login := CheckLogin(r); has_login == false {
		session.Values["success"] = false
		session.AddFlash("You haven't logged in.")
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
		return
	}
	// drain flashes
	session.Flashes()
	session.Save(r, w)
	// generate page
	user, err := controller.GetUser(bson.M{"email": session.Values["email"]})
	data := bson.M{
		"user": user,
	}
	err = templates.ExecuteTemplate(w, "order.html", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func MyOrderRate(w http.ResponseWriter, r *http.Request) {
	form := bson.M{
		"email":   r.FormValue("email"),
		"content": r.FormValue("content"),
		"rate":    r.FormValue("rate"),
		"id":      r.FormValue("id"),
	}
	err := controller.AddWorkshopRate(form)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/myorder", 301)
}

func Book(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "talent")
	// check login
	if has_login := CheckLogin(r); has_login == false {
		session.Values["success"] = false
		session.AddFlash("You haven't logged in.")
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
		return
	}
	// check param
	if CheckParam(r, "id") == false || CheckParam(r, "email") == false {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	// fetch data
	form := bson.M{
		"id":    r.FormValue("id"),
		"email": r.FormValue("email"),
	}
	err := controller.Book(form)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// drain flashes
	session.Flashes()
	session.Save(r, w)
	// generate page
	http.Redirect(w, r, "/workshop?id="+form["id"].(string), 301)
}

func Unbook(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "talent")
	// check login
	if has_login := CheckLogin(r); has_login == false {
		session.Values["success"] = false
		session.AddFlash("You haven't logged in.")
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
		return
	}
	// check param
	if CheckParam(r, "id") == false || CheckParam(r, "email") == false {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	// fetch data
	form := bson.M{
		"id":    r.FormValue("id"),
		"email": r.FormValue("email"),
	}
	err := controller.Unbook(form)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// drain flashes
	session.Flashes()
	session.Save(r, w)
	// generate page
	http.Redirect(w, r, "/workshop?id="+form["id"].(string), 301)
}

func Drop(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "talent")
	// check login
	if has_login := CheckLogin(r); has_login == false {
		session.Values["success"] = false
		session.AddFlash("You haven't logged in.")
		session.Save(r, w)
		http.Redirect(w, r, "/signup", 301)
		return
	}
	// check param
	if CheckParam(r, "id") == false || CheckParam(r, "email") == false {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	// fetch data
	form := bson.M{
		"id":    r.FormValue("id"),
		"email": r.FormValue("email"),
	}
	err := controller.Unbook(form)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// drain flashes
	session.Flashes()
	session.Save(r, w)
	// generate page
	http.Redirect(w, r, "/myorder", 301)
}

func CheckBooked(w http.ResponseWriter, r *http.Request) {
	form := bson.M{
		"email": r.FormValue("email"),
		"id":    r.FormValue("id"),
	}
	ret, err := controller.CheckBooked(form)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var js []byte
	js, err = json.Marshal(bson.M{"booked": ret})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/api/myorder/succeed", MyOrderSucceedData).Methods("GET")
	r.HandleFunc("/api/myorder/history", MyOrderHistoryData).Methods("GET")
	r.HandleFunc("/api/myorder/rate", MyOrderRate).Methods("POST")
	r.HandleFunc("/api/booked", CheckBooked).Methods("GET")
	r.HandleFunc("/book", Book).Methods("GET")
	r.HandleFunc("/unbook", Unbook).Methods("GET")
	r.HandleFunc("/drop", Drop).Methods("GET")
	r.HandleFunc("/myorder", MyOrderGet).Methods("GET")
	r.HandleFunc("/workshop", WorkShopGet).Methods("GET")
	r.HandleFunc("/api/landing", LandingData).Methods("GET")
	r.HandleFunc("/landing", LandingGet).Methods("GET")
	r.HandleFunc("/search", SearchGet).Methods("GET")
	r.HandleFunc("/api/search", SearchData).Methods("GET")
	r.HandleFunc("/login", LoginPost).Methods("POST")
	r.HandleFunc("/signup", SignupGet).Methods("GET")
	r.HandleFunc("/signup", SignupPost).Methods("POST")
	r.HandleFunc("/api/signup", SignupData).Methods("GET")
	r.HandleFunc("/verify", Verify).Methods("GET")
	r.HandleFunc("/logout", Logout).Methods("GET")
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("web/"))))

	http.Handle("/", r)
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		panic(err)
	}
}
