package controller

import (
	//"errors"
	"gopkg.in/mgo.v2/bson"
	"strconv"
	"talent/proto"
	"talent/services"
	"time"
)

/* Basic Operations */

func AddWorkshop(email string, workid bson.ObjectId, form bson.M) (err error) {
	//Not verify email here
	form["email"] = email
	form["workid"] = workid
	//Create work
	session := services.Connect()
	defer session.Close()
	err = services.Insert(session, "talent", "workshops", form)
	return
}

func AddWorkshopRate(form bson.M) (err error) {
	// update workshop review
	id := form["id"].(string)
	rate, _ := strconv.Atoi(form["rate"].(string))
	time_time := time.Now()
	time_string := time_time.Format(time.RFC1123)
	new_review := proto.Review{
		Email:   form["email"].(string),
		Content: form["content"].(string),
		Rate:    rate,
		Time: proto.Time{
			Time:   time_time,
			String: time_string,
		},
	}
	session := services.Connect()
	defer session.Close()
	workshop := proto.Workshop{}
	err = services.One(session, "talent", "workshops", bson.M{"_id": bson.ObjectIdHex(id)}, &workshop)
	has_review := false
	for i, review := range workshop.Reviews {
		if review.Email == new_review.Email {
			workshop.Reviews[i] = new_review
			has_review = true
			break
		}
	}
	if has_review == false {
		workshop.Reviews = append(workshop.Reviews, new_review)
	}
	err = services.Update(session, "talent", "workshops", bson.M{"_id": bson.ObjectIdHex(id)}, workshop)
	// update work popularity
	work := proto.Work{}
	err = services.One(session, "talent", "works", bson.M{"_id": workshop.Workid}, &work)
	sum := work.Popularity.Rate*float32(work.Popularity.Flow) + float32(new_review.Rate)
	work.Popularity.Flow += 1
	work.Popularity.Rate = sum / float32(work.Popularity.Flow)
	err = services.Update(session, "talent", "works", bson.M{"_id": workshop.Workid}, &work)
	return
}

func Book(form bson.M) (err error) {
	id := form["id"].(string)
	email := form["email"].(string)
	session := services.Connect()
	defer session.Close()
	//update workshop capacity
	workshop := proto.Workshop{}
	err = services.One(session, "talent", "workshops", bson.M{"_id": bson.ObjectIdHex(id)}, &workshop)
	workshop.Capacity.Current += 1
	err = services.Update(session, "talent", "workshops", bson.M{"_id": bson.ObjectIdHex(id)}, &workshop)
	//update user succeed
	user := proto.User{}
	err = services.One(session, "talent", "users", bson.M{"email": email}, &user)
	//add order
	time_time := time.Now()
	time_string := time_time.Format(time.RFC1123)
	order := proto.Order{
		ID: workshop.ID,
		Time: proto.Time{
			Time:   time_time,
			String: time_string,
		},
	}
	user.Succeed = append(user.Succeed, order)
	err = services.Update(session, "talent", "users", bson.M{"email": email}, &user)
	return
}

func Unbook(form bson.M) (err error) {
	id := form["id"].(string)
	email := form["email"].(string)
	session := services.Connect()
	defer session.Close()
	//update workshop capacity
	workshop := proto.Workshop{}
	err = services.One(session, "talent", "workshops", bson.M{"_id": bson.ObjectIdHex(id)}, &workshop)
	workshop.Capacity.Current -= 1
	err = services.Update(session, "talent", "workshops", bson.M{"_id": bson.ObjectIdHex(id)}, &workshop)
	//update user succeed
	user := proto.User{}
	err = services.One(session, "talent", "users", bson.M{"email": email}, &user)
	//remove order
	for i, order := range user.Succeed {
		if order.ID.Hex() == id {
			user.Succeed = append(user.Succeed[:i], user.Succeed[i+1:]...)
			break
		}
	}
	err = services.Update(session, "talent", "users", bson.M{"email": email}, &user)
	return
}

func CheckBooked(form bson.M) (booked bool, err error) {
	id := form["id"].(string)
	email := form["email"].(string)
	session := services.Connect()
	defer session.Close()
	//check user succeed
	user := proto.User{}
	err = services.One(session, "talent", "users", bson.M{"email": email}, &user)
	booked = false
	for _, order := range user.Succeed {
		if order.ID.Hex() == id {
			booked = true
		}
	}
	return
}

//data
func GetWorkshop(form bson.M) (workshop bson.M, err error) {
	id := form["id"].(string)
	session := services.Connect()
	defer session.Close()
	err = services.One(session, "talent", "workshops", bson.M{"_id": bson.ObjectIdHex(id)}, &workshop)
	workshop["work"] = bson.M{}
	err = services.One(session, "talent", "works", bson.M{"_id": workshop["workid"]}, workshop["work"])
	workshop["owner"] = bson.M{}
	err = services.One(session, "talent", "users", bson.M{"email": workshop["work"].(bson.M)["email"]}, workshop["owner"])
	return
}

func RemoveWorkshop(id bson.ObjectId) (err error) {
	session := services.Connect()
	defer session.Close()
	err = services.Remove(session, "talent", "workshops", bson.M{"_id": id})
	return
}

// func main() {
// 	workshop := proto.Workshop{}
// 	session := services.Connect()
// 	defer session.Close()
// 	services.One(session, "talent", "workshops", bson.M{"_id": bson.ObjectIdHex("5531a238a39752a0c2fc2f97")}, &workshop)
// 	workshop.Length = 240
// 	services.Update(session, "talent", "workshops", bson.M{"_id": bson.ObjectIdHex("5531a238a39752a0c2fc2f97")}, &workshop)
// }
