package controller

import (
	"gopkg.in/mgo.v2/bson"
	"strconv"
	"strings"
	//"talent/proto"
	"talent/services"
)

func query2keywords(query string) (keywords []string) {
	keywords = strings.Fields(query)
	return
}
func InQuery(valueList interface{}) bson.M {
	return bson.M{"$in": valueList}
}
func OrQuery(valueList []bson.M) bson.M {
	return bson.M{"$or": valueList}
}
func FuzzyQuery(value interface{}) bson.M {
	return bson.M{"$regex": bson.RegEx{value.(string), "i"}}
}

//data
func SelectWorkshops(form bson.M) (result bson.M, err error) {
	query := form["query"].(string)
	sort := form["sort"].([]string)
	offset, _ := strconv.Atoi(form["offset"].(string))
	size, _ := strconv.Atoi(form["size"].(string))
	session := services.Connect()
	defer session.Close()
	//generate keywords through query
	keywords := query2keywords(query)
	//get aggregate ids
	results := []bson.M{}
	for _, keyword := range keywords {
		tmp := []bson.M{}
		orquery := OrQuery([]bson.M{bson.M{"name": FuzzyQuery(keyword)},
			bson.M{"outline": FuzzyQuery(keyword)}})
		err = services.All(session, "talent", "works", orquery, &tmp)
		//log.Println(tmp)
		results = append(results, tmp...)
	}
	ids := services.Group(results)
	//fetch results
	total, _ := services.Count(session, "talent", "workshops", bson.M{"workid": InQuery(ids)})
	workshops := []bson.M{}
	err = services.Search(session, "talent", "workshops", bson.M{"workid": InQuery(ids)}, sort, offset, size, &workshops)
	for _, workshop := range workshops {
		workshop["work"] = bson.M{}
		services.One(session, "talent", "works", bson.M{"_id": workshop["workid"]}, workshop["work"])
		workshop["owner"] = bson.M{}
		services.One(session, "talent", "users", bson.M{"email": workshop["work"].(bson.M)["email"]}, workshop["owner"])
	}
	result = bson.M{"result": workshops, "total": total}
	return
}

//data
func NewWorkshops(form bson.M) (result bson.M, err error) {
	sort := form["sort"].([]string)
	offset, _ := strconv.Atoi(form["offset"].(string))
	size, _ := strconv.Atoi(form["size"].(string))
	session := services.Connect()
	defer session.Close()
	//fetch results
	total, _ := services.Count(session, "talent", "workshops", nil)
	workshops := []bson.M{}
	err = services.Search(session, "talent", "workshops", nil, sort, offset, size, &workshops)
	for _, workshop := range workshops {
		workshop["work"] = bson.M{}
		services.One(session, "talent", "works", bson.M{"_id": workshop["workid"]}, workshop["work"])
		workshop["owner"] = bson.M{}
		services.One(session, "talent", "users", bson.M{"email": workshop["work"].(bson.M)["email"]}, workshop["owner"])
	}
	result = bson.M{"result": workshops, "total": total}
	return
}
