package controller

import (
	//"errors"
	"gopkg.in/mgo.v2/bson"
	//"strings"
	"os/exec"
	"talent/proto"
	"talent/services"
	//"time"
)

func AddWork(email string, form bson.M) (err error) {
	//Not verify email here
	form["email"] = email
	//Create work
	session := services.Connect()
	defer session.Close()
	err = services.Insert(session, "talent", "works", form)
	// get id out
	work := proto.Work{}
	services.One(session, "talent", "works", form, &work)
	// add storage
	cmd := exec.Command("mkdir", "-p", "web/img/works/"+work.ID.Hex()+"/examples")
	err = cmd.Run()
	return
}

//data
func GetWork(id string) (work proto.Work, err error) {
	session := services.Connect()
	defer session.Close()
	err = services.One(session, "talent", "works", bson.M{"_id": bson.ObjectIdHex(id)}, &work)
	return
}

func RemoveWork(id string) (err error) {
	session := services.Connect()
	defer session.Close()
	err = services.Remove(session, "talent", "works", bson.M{"_id": bson.ObjectIdHex(id)})
	// remove storage
	if id != "" {
		cmd := exec.Command("rm", "-rf", "web/img/works/"+id)
		err = cmd.Run()
	}
	return
}

// func main() {
// 	//RemoveWork("5522a3c2a39752a0c2fc2f8a")
// 	form := bson.M{
// 		"name":           "Web app creation using golang and angularjs",
// 		"outline":        "In class we will create application using two most popular techs golang and angularjs.",
// 		"objectives":     []string{"Let students know about golang morzilla framework", "Let them understand angularjs", "Combine those two to make a brief app"},
// 		"preparations":   []string{"Laptop with golang, angularjs, mongodb and uikit installed"},
// 		"certifications": []string{"CMU geek, graduate student", "4-year experience in programming"},
// 		"examples":       3,
// 		"rate":           5,
// 	}
// 	AddWork("hl2839@columbia.edu", form)
// }
