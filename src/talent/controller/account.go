package controller

import (
	"crypto/md5"
	"errors"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	//"log"
	"os/exec"
	"strconv"
	"strings"
	"talent/proto"
	"talent/services"
	"time"
)

func CheckEmail(form bson.M) (message string, code int) {
	//check identity
	if tokens := strings.Split(form["email"].(string), "@"); len(tokens) != 2 {
		message = "Invalid email address."
		code = 0
		return
	} else {
		cu_id := tokens[0]
		org := tokens[1]
		if org != "columbia.edu" {
			message = "Not columbia email address."
			code = 0
			return
		}
		fullname, err := services.ScrapeOne("https://directory.columbia.edu/people/uni?code="+cu_id, "//div[@class='table_results_indiv']//tbody/tr[1]/th")
		if err != nil || fullname == "" {
			message = "Inexistent columbia email address."
			code = 0
			return
		}
	}

	session := services.Connect()
	defer session.Close()
	if cnt, _ := services.Count(session, "talent", "users", bson.M{"email": form["email"]}); cnt > 0 {
		user := bson.M{}
		services.One(session, "talent", "users", bson.M{"email": form["email"]}, user)
		message = "Welcome " + user["firstname"].(string) + "!"
		code = 2
		return
	}
	message = "Valid Columbia U email address."
	code = 1
	return
}

func Signup(form bson.M) (err error) {
	//check identity
	if tokens := strings.Split(form["email"].(string), "@"); len(tokens) != 2 {
		err = errors.New("Invalid email address.")
		return
	} else {
		cu_id := tokens[0]
		fullname := ""
		fullname, err = services.ScrapeOne("https://directory.columbia.edu/people/uni?code="+cu_id, "//div[@class='table_results_indiv']//tbody/tr[1]/th")
		if err != nil || fullname == "" {
			err = errors.New("Inexistent columbia email address")
			return
		} else {
			tokens = strings.Split(fullname, " ")
			form["firstname"] = tokens[0]
			lastname := ""
			for i := 1; i < len(tokens); i++ {
				lastname = lastname + " " + tokens[i]
			}
			form["lastname"] = lastname
		}
	}

	session := services.Connect()
	defer session.Close()
	if cnt, _ := services.Count(session, "talent", "users", bson.M{"email": form["email"]}); cnt > 0 {
		err = errors.New("This email address has already been registered.")
		return
	}
	//md5
	password := []byte(form["password"].(string))
	form["password"] = fmt.Sprintf("%x", md5.Sum(password))
	err = services.Insert(session, "talent", "profiles", form)
	if err != nil {
		return
	}
	profile := bson.M{}
	err = services.One(session, "talent", "profiles", form, profile)
	if err != nil {
		return
	}
	err = services.Verify(profile["_id"].(bson.ObjectId).Hex(), "qicongc@cs.cmu.edu", profile["firstname"].(string))
	return
}

func Signin(form bson.M) error {
	session := services.Connect()
	defer session.Close()
	//md5
	password := []byte(form["password"].(string))
	form["password"] = fmt.Sprintf("%x", md5.Sum(password))
	if cnt, _ := services.Count(session, "talent", "users", form); cnt == 1 {
		return nil
	} else {
		return errors.New("No such email or wrong password.")
	}
}

func Verify(form bson.M) (err error) {
	session := services.Connect()
	defer session.Close()
	profile := bson.M{}
	err = services.One(session, "talent", "profiles", bson.M{"_id": bson.ObjectIdHex(form["token"].(string))}, profile)
	if err != nil {
		return
	}
	if profile["_id"] == nil {
		err = errors.New("Link is not valid now.")
		return
	}
	//insert to users table
	user := proto.User{
		Email:     profile["email"].(string),
		Password:  profile["password"].(string),
		Firstname: profile["firstname"].(string),
		Lastname:  profile["lastname"].(string),
		Begin:     time.Now(),
	}
	err = services.Insert(session, "talent", "users", &user)
	if err != nil {
		return
	}
	//remove from profiles table
	err = services.Remove(session, "talent", "profiles", bson.M{"email": user.Email})
	if err != nil {
		return
	}
	//add storage
	cmd := exec.Command("mkdir", "-p", "web/img/users/"+user.Email)
	err = cmd.Run()
	return
}

//data
func GetUser(form bson.M) (user bson.M, err error) {
	email := form["email"].(string)
	session := services.Connect()
	defer session.Close()
	err = services.One(session, "talent", "users", bson.M{"email": email}, &user)
	return
}

//data
func GetSucceed(form bson.M) (orders []bson.M, err error) {
	email := form["email"].(string)
	offset, _ := strconv.Atoi(form["offset"].(string))
	size, _ := strconv.Atoi(form["size"].(string))
	session := services.Connect()
	defer session.Close()
	//fetch results
	user := proto.User{}
	err = services.One(session, "talent", "users", bson.M{"email": email}, &user)

	end := len(user.Succeed)
	if offset >= end {
		return
	}
	if offset+size < end {
		end = offset + size
	}
	for _, order := range user.Succeed[offset:end] {
		workshop := bson.M{}
		err = services.One(session, "talent", "workshops", bson.M{"_id": order.ID}, workshop)
		workshop["work"] = bson.M{}
		err = services.One(session, "talent", "works", bson.M{"_id": workshop["workid"]}, workshop["work"])
		workshop["owner"] = bson.M{}
		err = services.One(session, "talent", "users", bson.M{"email": workshop["work"].(bson.M)["email"]}, workshop["owner"])
		orders = append(orders, bson.M{"orderby": order.Time, "workshop": workshop})
	}
	return
}

//data
func GetHistory(form bson.M) (orders []bson.M, err error) {
	email := form["email"].(string)
	offset, _ := strconv.Atoi(form["offset"].(string))
	size, _ := strconv.Atoi(form["size"].(string))
	session := services.Connect()
	defer session.Close()
	//fetch results
	user := proto.User{}
	err = services.One(session, "talent", "users", bson.M{"email": email}, &user)

	end := len(user.History)
	if offset >= end {
		return
	}
	if offset+size < end {
		end = offset + size
	}
	for _, order := range user.History[offset:end] {
		workshop := bson.M{}
		err = services.One(session, "talent", "workshops", bson.M{"_id": order.ID}, workshop)
		workshop["work"] = bson.M{}
		err = services.One(session, "talent", "works", bson.M{"_id": workshop["workid"]}, workshop["work"])
		workshop["owner"] = bson.M{}
		err = services.One(session, "talent", "users", bson.M{"email": workshop["work"].(bson.M)["email"]}, workshop["owner"])
		orders = append(orders, bson.M{"orderby": order.Time, "workshop": workshop})
	}
	return
}

// func main() {
// 	user := proto.User{}
// 	session := services.Connect()
// 	defer session.Close()
// 	services.One(session, "talent", "users", bson.M{"email": "hl2839@columbia.edu"}, &user)
// 	time_string := "Sat, 18 April 2015 14:17:00 EDT"
// 	time_time, _ := time.Parse(time.RFC1123, time_string)
// 	order := proto.Order{
// 		ID: bson.ObjectIdHex("55306f19a39752a0c2fc2f94"),
// 		Time: proto.Time{
// 			Time:   time_time,
// 			String: time_string,
// 		},
// 	}
// 	user.History = []proto.Order{order}
// 	services.Update(session, "talent", "users", bson.M{"email": "hl2839@columbia.edu"}, &user)
// }

//Anna: liqihuimm@gmail
//Sai: msaisumanth@gmail
//Qiang:abbyju37@gmail.com
