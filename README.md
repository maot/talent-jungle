# Talent Jungle #

### How do I get set up? ###

```
#!bash
#install mongodb
brew update
brew install mongodb
#install mercurial
brew install mercurial
#launch mongod
sudo mongod
#import database
mongorestore -d talent ./talent-jungle/mongo
#install go if you have not
#find pkg at https://storage.googleapis.com/golang/go1.4.2.darwin-amd64-osx10.8.pkg
#set gopath to talent jungle
export GOPATH=$(pwd)/talent-jungle
#compile project
cd talent-jungle/src/talent/server
go get
#launch server
cd $GOPATH
sudo ./bin/server
#see our website on browser
#localhost/landing

```
