'use strict';

/* Services */

angular.module('talentServices', []).factory('Paginator', function() {
  return function(fetchFunction, pageSize) {
    var paginator = {
      hasNextVar: false,
      next: function() {
        if (this.hasNextVar) {
          this.currentOffset += pageSize;
          this._load();
        }
      },
      _load: function() {
        var self = this;
        fetchFunction(this.currentOffset, pageSize + 1, function(items) {
          console.log(items);
          self.totalItems = items.total;
          self.totalPages = Math.floor(items.total / pageSize) + 1;
          items = items.result;
          self.currentPageItems = items.slice(0, pageSize);
          self.hasNextVar = items.length === pageSize + 1;
        });
      },
      hasNext: function() {
        return this.hasNextVar;
      },
      previous: function() {
        if(this.hasPrevious()) {
          this.currentOffset -= pageSize;
          this._load();
        }
      },
      hasPrevious: function() {
        return this.currentOffset !== 0;
      },
      currentPageItems: [],
      currentOffset: 0,
      totalItems: 0,
      totalPages: 0
    };

    paginator._load();
    return paginator;
  };
});
