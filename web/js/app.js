'use strict';

/* App Module */

var talentApp = angular.module('talentApp', [
    'talentServices'
    ]);

talentApp.config(function($sceProvider) {
  $sceProvider.enabled(false);
});
